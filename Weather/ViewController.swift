//
//  ViewController.swift
//  Weather
//
//  Created by Doan Huy Binh on 10/1/16.
//  Copyright © 2016 DiffCat. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    //Initialize Control
    
    @IBOutlet weak var CityLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var cloudConverLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var raininlast3hLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var weatherImageIcon: UIImageView!
    
    @IBOutlet weak var weatherActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var CityText: UITextField!
    
    //Header Control
    @IBOutlet weak var cityHeaderLabel: UILabel!
    @IBOutlet weak var weatherHeaderLabel: UILabel!
    @IBOutlet weak var temperatureHeaderLabel: UILabel!
    @IBOutlet weak var cloudCoverHeaderLabel: UILabel!
    @IBOutlet weak var windHeaaderLabel: UILabel!
    @IBOutlet weak var rainInLast3hHeaderLabel: UILabel!
    @IBOutlet weak var humidityHeaderLabel: UILabel!
    @IBOutlet weak var imageWeatherHeaderLabel: UILabel!
    
    
    var openWeatherMapBaseURL = "http://api.openweathermap.org/data/2.5/forecast/city?id="
    var openWeatherMapAPIKey: String = "d42bab5e7bc5f8ccc338b2e22753db02"
    
    func getWeatherInfo(city: String)
    {
        weatherActivityIndicator.isHidden = false
        weatherActivityIndicator.startAnimating()
        if city != ""
        {
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession.init(configuration: sessionConfig)
            let weatherRequestURL = URL(string: "\(openWeatherMapBaseURL)\(city)&APPID=\(openWeatherMapAPIKey)")
            let dataTask = session.dataTask(with: weatherRequestURL!, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
                if error == nil
                {
                    do{
                        let weatherData = try JSONSerialization.jsonObject(with: data!, options:  JSONSerialization.ReadingOptions.mutableContainers) as! [String: AnyObject]
                        let nameCity = weatherData["city"]!["name"]!! as! String
                        self.CityLabel.text = nameCity
                        
                        
                        let list = weatherData["list"]![0] as! NSDictionary
                        
                        let weather = list.object(forKey: "weather")! as! NSArray
                        let description = (weather[0] as! NSDictionary)["main"] as! String
                        self.weatherLabel.text = description
                        
                        let main = list.object(forKey: "main") as! NSDictionary
                        let temp = Int((main["temp"] as! Double) - 273.15)
                        self.temperatureLabel.text = "\(temp)°C"
                        
                        //Cloud Cover
                        
                        let cloud = list.object(forKey: "clouds") as! NSDictionary
                        let cloudValue = cloud["all"]
                        self.cloudConverLabel.text = "\(cloudValue!)%"
                        //Wind Speed
                        
                        let wind = list.object(forKey: "wind") as! NSDictionary
                        self.windLabel.text = "\(wind["speed"]!)m/s"
                        
                        //Rain
                        let rain = list.object(forKey: "rain") as! NSDictionary
                        
                        if rain != nil
                        {
                            self.raininlast3hLabel.text = "\(rain["3h"]!)mm"
                        }
                        else
                        {
                            self.raininlast3hLabel.text = "none"
                        }
                        //humidity
                        self.humidityLabel.text = "\(main["humidity"]!)%"
                        
                        //URL ICON :http://openweathermap.org/img/w/
                        
                        let iconPathURL = URL(string:"http://openweathermap.org/img/w/\(main["icon"])" )
                        
                        let iconData = try Data(contentsOf: iconPathURL!)
                        //print(iconPathURL)
                        self.weatherImageIcon.image = UIImage(data: iconData)
                        
                        
                        self.weatherActivityIndicator.isHidden = true
                        self.weatherActivityIndicator.stopAnimating()
                       
                        
                    }
                    catch _
                    {
                        print ("Error Loading Weather Info")
                        
                    }
                }
                else
                {
                    print("Error")
                }
               
                
            })
            dataTask.resume()
        }
    }
    
    
    @IBAction func getWeatherPress(_ sender: AnyObject) {
        
        if CityText.text != ""
        {
            self.view.endEditing(true)
            getWeatherInfo(city: CityText.text!)

            
        }
        else
        {
            let alertController = UIAlertController(title: "Error", message: "Please, Provide some content", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        CityText.resignFirstResponder()
        return true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CityText.delegate = self
        CityText.text = "6058560"
        CityLabel.text = ""
        weatherLabel.text = ""
        temperatureLabel.text = ""
        cloudConverLabel.text = ""
        windLabel.text = ""
        raininlast3hLabel.text = ""
        humidityLabel.text = ""
        
        weatherActivityIndicator.isHidden = true
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    
    
    
    var kbHeight : CGFloat!
    
    func keyboardWillShow(notification: NSNotification)
    {
        if let userinfo = notification.userInfo
        {
            if let keyboardSize = (userinfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
            {
                if view.frame.origin.y == 0
                {
                    self.view.frame.origin.y = view.frame.origin.y - keyboardSize.height
                }
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification){
        
        if let userInfo = notification.userInfo
        {
            if let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
            {
                if view.frame.origin.y != 0
                {
                    self.view.frame.origin.y = view.frame.origin.y + keyboardSize.height
                }
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

